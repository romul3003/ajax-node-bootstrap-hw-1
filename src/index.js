import 'bootstrap/js/dist/collapse'
import './css/index.css'
import './less/index.less'
import './scss/index.scss'

import './modules/XmlHTTPRequest'
import './modules/FetchRequest'