import Api from './Api'

const fetchBtn = document.querySelector('[data-element="fetchRequest"]')

fetchBtn.addEventListener('click', handleFetchBtn)

async function handleFetchBtn(event) {
	const ajax = new Api()

	const response = await ajax.fetchData('https://swapi.co/api/films/')
	const data = await response.json()
	data.requestName = event.target.dataset.element

	renderAccordion(data)
}

function renderAccordion({results, requestName}) {
	const renderedId = Math.random().toFixed(7).toString().slice(2)
	const container = document.querySelector(`#${requestName}`)

	let ul = container.querySelector('.accordion')
	if (ul) ul.remove()

	ul = document.createElement('ul')
	ul.className = `accordion`
	ul.id = `accordion-${renderedId}`
	container.append(ul)

	results.sort((a, b) => a.episode_id - b.episode_id)

	results.forEach((item, index) => {
		const li = document.createElement('li')
		li.classList.add('card')
		const {characters, title, episode_id: id, opening_crawl: description} = item

		li.innerHTML = `
			<div class="card-header" id="heading${renderedId}-${id}">
				<h5 class="mb-0">
					<button 
						class="btn btn-link" 
						type="button" 
						data-toggle="collapse" 
						data-target="#collapse${renderedId}-${id}" 
						aria-expanded="true" 
						aria-controls="collapse${renderedId}-${id}"
					>
					Episode ${id}: ${title}
					</button>
					
				</h5>
			</div>
			
			<div 
				id="collapse${renderedId}-${id}" 
				class="collapse  ${!index ? 'show' : ''}" 
				aria-labelledby="heading${renderedId}-${id}" 
				data-parent="#accordion-${renderedId}">
				
				<div class="card-body">
					<div class="description">${description}</div>
					<h5 class="characters">Characters: </h5>
					<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
				</div>
				
			</div>
		`
		ul.append(li)

		const charactersContainer = document.createElement('ul')
		charactersContainer.classList.add('characters')
		li.querySelector('.card-body').append(charactersContainer)

		let requests = []

		for (let i = 0; i < characters.length; i++) {
			requests.push(new Api().fetchData(characters[i]))
		}

		Promise.all(requests)
			.then(responses => Promise.all(responses.map(r => r.json())))
			.then(characters => {
				const spinner = li.querySelector('.lds-spinner')
				if (spinner) spinner.classList.add('d-none')

				charactersContainer.innerHTML = characters
					.map(character => `<li>name: ${character.name}, height: ${character.height}, mass: ${character.mass}, hair color: ${character.hair_color}, skin color: ${character.skin_color}</li>`)
					.join('')
			})
	})
}
