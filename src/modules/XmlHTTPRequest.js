import Api from './Api'

const xmlHttpRequestBtn = document.querySelector('[data-element="XMLHttpRequest"]')

xmlHttpRequestBtn.addEventListener('click', handleXmlHttpRequestBtn)

function handleXmlHttpRequestBtn() {
	const ajax = new Api()

	ajax
		.getData(
			'https://swapi.co/api/films/',
			onFilmHandler
		)
}


function onFilmHandler(data) {
	const {results} = data
	const container = document.querySelector('#xmlHttpRequest')

	const ajax = new Api()

	let ul = container.querySelector('.accordion--1')
	if (ul) ul.remove()

	ul = document.createElement('ul')
	ul.className = 'accordion accordion--1'
	ul.id = 'accordion'
	container.append(ul)

	results.sort((a, b) => a.episode_id - b.episode_id)

	results.forEach((item, index) => {
		const li = document.createElement('li')
		li.classList.add('card')
		const {characters, title, episode_id: id, opening_crawl: description} = item

		li.innerHTML = `
			<div class="card-header" id="heading${id}">
				<h5 class="mb-0">
					<button 
						class="btn btn-link" 
						type="button" 
						data-toggle="collapse" 
						data-target="#collapse${id}" 
						aria-expanded="true" 
						aria-controls="collapse${id}"
					>
					Episode ${id}: ${title}
					</button>
					
				</h5>
			</div>
			
			<div 
				id="collapse${id}" 
				class="collapse  ${!index ? 'show': ''}" 
				aria-labelledby="heading${id}" 
				data-parent="#accordion">
				
				<div class="card-body">
					<div class="description">${description}</div>
					<h5 class="characters">Characters: </h5>
					<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
				</div>
				
			</div>
		
		`
		ul.append(li)

		const charactersContainer = document.createElement('ul')
		charactersContainer.classList.add('characters')
		li.querySelector('.card-body').append(charactersContainer)

		let arrReq = []
		for (let i = 0; i < characters.length; i++) {
			arrReq.push(
				ajax.getData(
					characters[i],
					character => {}
				)
			)
		}

		Api.all(arrReq, function (data) {
			const spinner =  li.querySelector('.lds-spinner')
			if (spinner) spinner.classList.add('d-none')

			charactersContainer.innerHTML = data
				.map(character => `<li>name: ${character.name}, height: ${character.height}, mass: ${character.mass}, hair color: ${character.hair_color}, skin color: ${character.skin_color}</li>`)
				.join('')
		})
	})
}
