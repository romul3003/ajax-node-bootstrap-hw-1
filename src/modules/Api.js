class Api {

	getData(url, callback) {
		const req = new XMLHttpRequest()

		req.open('GET', url)
		req.onload = function () {
			callback(req.response)
		}
		req.onerror = function () {
			console.log("Ошибка HTTP: " + req.status)
		}
		req.responseType = 'json'
		req.send()

		return req
	}

	static all(requests, callback) {
		let count = 0
		const data = []

		requests.forEach(request => {
			request.addEventListener('load', function () {
					count++;
					data.push(request.response)

					if (requests.length === count) {
						callback(data)
					}
				}
			)

		})
	}

	async fetchData(url) {
		let response = await fetch(url)
		if (response.ok) {
			return response
		} else {
			console.log("Ошибка HTTP: " + response.status)
		}
	}
}

export default Api